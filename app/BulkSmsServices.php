<?php

namespace App;

class BulkSmsServices  
{
    protected $api_token,
              $to,
              $body,
              $from;


    public function send($api_token, $from, $body, $numbers = [])
    {
        $this->api_token = $api_token;
        $this->from  = $from;
        $this->body = $body;

        $x = 1;
        $set = '';

        foreach ($numbers as $number) {
            $set .= "{$number}";
            if($x < count($numbers)){
                $set .= ',';
            }
            $x++;
        }
        $this->to = $set;

        $this->sendRequest();
    }

    public function sendRequest()
    {
        $body = \urlencode($this->body);
        $url = "https://www.bulksmsnigeria.com/api/v1/sms/create?api_token={$this->api_token}&from={$this->from}&to={$this->to}&body={$body}";
        // echo $url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result= curl_exec($ch);
        curl_close($ch);
        $r = json_decode($result);
        return $r;
    }

    
}
